"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uuidv4 = void 0;
const uuidv4 = () => {
    return new Date().getTime();
};
exports.uuidv4 = uuidv4;

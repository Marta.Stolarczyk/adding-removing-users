"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.actions = void 0;
exports.actions = {
    REMOVE_USER: "action-remove-user",
    TOGGLE_USER: "action-toggle-user",
};

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersTable = void 0;
const actions_js_1 = require("../actions.js");
class UsersTable {
    constructor(container, state) {
        this.container = container;
        this.state = state;
        this.render();
        this.tbody = this.container.querySelector("tbody");
        this.renderTable();
        this.state.setChangeCallback(this.renderTable.bind(this));
        this.addListeners();
        this.renderUserPosts();
    }
    render() {
        this.container.innerHTML = `
    <div class="my-3">
      <h3>Table actions</h3>
      <button class="btn btn-primary">Action 1</button>
      <button class="btn btn-primary">Action 2</button>
      <button class="btn btn-primary">Action 3</button>
    </div>

    <table class="table table-bordered">
      <thead>
        <tr>
          <th><input type="checkbox"></th>
          <th>Name</th>
          <th>Username</th>
          <th>Posts</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>`;
    }
    renderTable() {
        const users = this.state.users;
        this.tbody.innerHTML = "";
        const rows = users.map((user) => {
            const tr = document.createElement("tr");
            const isChecked = user.checked ? "checked" : "";
            tr.innerHTML = `
        <td><input type="checkbox" ${actions_js_1.actions.TOGGLE_USER}="${user.id}" ${isChecked} /></td>
        <td>${user.name}</td>
        <td>${user.username}</td>
        <td><button data-show-user-post="${user.id}" class="btn btn-outline-danger"><i class="bi bi-chat-dots-fill"></i></button></td>
        <td><button ${actions_js_1.actions.REMOVE_USER}="${user.id}" class="btn btn-danger"><i class="bi bi-trash"></i></button></td>
      `;
            return tr;
        });
        this.tbody.append(...rows);
    }
    addListeners() {
        this.tbody.addEventListener("click", (event) => __awaiter(this, void 0, void 0, function* () {
        }));
    }
    renderUserPosts() {
        // Implementation for rendering user posts
    }
}
exports.UsersTable = UsersTable;

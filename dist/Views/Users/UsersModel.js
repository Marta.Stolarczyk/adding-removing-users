"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersModel = void 0;
class UsersModel {
    constructor(initialState) {
        this.users = initialState.users || [];
        this.removeId = initialState.removeId || null;
        this.posts = initialState.posts || [];
        this.onUsersChange = null;
        this.onUsersPosts = null;
    }
    bindUsersChange(callback) {
        if (typeof callback === "function") {
            this.onUsersChange = callback;
        }
    }
    bindUsersPosts(callback) {
        if (typeof callback === "function") {
            this.onUsersPosts = callback;
        }
    }
    setUsers(users) {
        this.users = users;
        if (this.onUsersChange) {
            this.onUsersChange(this.users);
        }
    }
    setRemoveId(removeId) {
        this.removeId = removeId;
    }
    setPosts(posts) {
        this.posts = posts;
        if (this.onUsersPosts) {
            this.onUsersPosts(this.posts);
        }
    }
    getPostsByUserId(userId) {
        return this.posts.filter(post => post.userId === userId);
    }
}
exports.UsersModel = UsersModel;

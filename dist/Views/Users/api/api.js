"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchPost = exports.fetchData = void 0;
const error_js_1 = require("../../../error/error.js");
const mapUsers_js_1 = require("./mapUsers.js");
const fetchData = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const res = yield fetch("https://jsonplaceholder.typicode.com/users");
        if (!res.ok) {
            throw new Error("Something went wrong!");
        }
        const data = yield res.json();
        const users = (0, mapUsers_js_1.mapUsers)(data);
        return users;
    }
    catch (error) {
        (0, error_js_1.handleError)(error);
    }
});
exports.fetchData = fetchData;
const fetchPost = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield fetch(`https://jsonplaceholder.typicode.com/posts`);
        if (!response.ok) {
            throw new Error("Something went wrong!");
        }
        const data = yield response.json();
        return data;
    }
    catch (error) {
        (0, error_js_1.handleError)(error);
    }
});
exports.fetchPost = fetchPost;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapUsers = void 0;
const mapUsers = (users) => {
    const mapedUsers = users.map((user) => {
        return Object.assign(Object.assign({}, user), { checked: false });
    });
    return mapedUsers;
};
exports.mapUsers = mapUsers;

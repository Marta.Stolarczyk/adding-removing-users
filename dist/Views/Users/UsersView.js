"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersView = void 0;
const id_generator_js_1 = require("../../utils/id-generator.js");
const actions_js_1 = require("./actions.js");
class UsersView {
    constructor({ appContainer, model }) {
        this.render = () => {
            const nav = `
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item" role="presentation">
        <button
          class="nav-link active"
          id="pills-table-tab"
          data-bs-toggle="pill"
          data-bs-target="#pills-table"
          type="button"
          role="tab"
          aria-controls="pills-table"
          aria-selected="true"
        >
          table
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button
          class="nav-link"
          id="pills-form-tab"
          data-bs-toggle="pill"
          data-bs-target="#pills-form"
          type="button"
          role="tab"
          aria-controls="pills-form"
          aria-selected="false"
        >
          form
        </button>
      </li>
    </ul>
    `;
            const tabContent = `
    <div class="tab-content" id="pills-tab-content">
      <div
        class="tab-pane fade show active"
        id="pills-table"
        role="tabpanel"
        aria-labelledby="pills-table-tab"
        tabindex="0"
      >
        <div class="my-3">
          <h3>Table actions</h3>
          <button class="btn btn-primary">Action 1</button>
          <button class="btn btn-primary">Action 2</button>
          <button class="btn btn-primary">Action 3</button>
        </div>

        <table class="table table-bordered">
          <thead>
            <tr>
              <th><input type="checkbox"></th>
              <th>Name</th>
              <th>Username</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>

      <div
        class="tab-pane fade"
        id="pills-form"
        role="tabpanel"
        aria-labelledby="pills-form-tab"
        tabindex="0"
      >
        <h3>Add new user</h3>

        <form id="add-user-form">
          <div class="row">
            <div class="col-3">
              <input type="text" class="form-control" id="input-name" placeholder="Name" required />
            </div>
            <div class="col-3">
              <input
                type="text"
                class="form-control"
                id="input-username"
                placeholder="Username"
                required
              />
            </div>
          </div>

          <div class="row mt-3">
            <div class="col">
              <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>`;
            const deleteModal = `
    <div class="modal" tabindex="-1" id="delete-user-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Potwierdzenie usunięcia użytkownika</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div class="modal-body">
            <p>Czy napewno chcesz usunąć tego użytkownika?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Anuluj</button>
            <button type="button" class="btn btn-primary" id="confirm-delete">Potwierdź</button>
          </div>
        </div>
      </div>
    </div>`;
            this.app.innerHTML = nav + tabContent + deleteModal;
        };
        this.renderTable = (users) => {
            const tbody = this.elements.tbody;
            tbody.innerHTML = "";
            const rows = users.map((user) => {
                const tr = document.createElement("tr");
                const isChecked = user.checked ? "checked" : "";
                tr.innerHTML = `
        <td><input type="checkbox" ${actions_js_1.actions.TOGGLE_USER}="${user.id}" ${isChecked} /></td>
        <td>${user.name}</td>
        <td>${user.username}</td>
        <td>
        <button data-show-user-post="${user.id}" class="btn btn-outline-danger"><i class="bi bi-chat-dots-fill"></i></button>
        <button ${actions_js_1.actions.REMOVE_USER}="${user.id}" class="btn btn-danger"><i class="bi bi-trash"></i></button>
        </td>
      `;
                return tr;
            });
            tbody.append(...rows);
        };
        this.bindToggleUser = (callback) => {
            this.elements.tbody.addEventListener("click", (event) => {
                const button = event.target.closest(`[${actions_js_1.actions.TOGGLE_USER}]`);
                if (button === null) {
                    return;
                }
                const id = Number(button.getAttribute(actions_js_1.actions.TOGGLE_USER));
                callback(id);
            });
        };
        this.bindRemoveUser = (callback) => {
            this.elements.tbody.addEventListener("click", (event) => {
                const button = event.target.closest(`[${actions_js_1.actions.REMOVE_USER}]`);
                if (button === null) {
                    return;
                }
                const id = Number(button.getAttribute(actions_js_1.actions.REMOVE_USER));
                callback(id);
            });
        };
        this.bindConfirmRemove = (callback) => {
            this.elements.confirmDeleteButton.addEventListener("click", callback);
        };
        this.bindAddUser = (callback) => {
            this.elements.form.addEventListener("submit", (event) => {
                event.preventDefault();
                const name = this.elements.inputName.value;
                const username = this.elements.inputUsername.value;
                const newUser = {
                    id: (0, id_generator_js_1.uuidv4)(),
                    name,
                    username,
                    checked: false,
                };
                callback(newUser);
            });
        };
        this.bindShowUserPosts = (callback) => {
            this.elements.tbody.addEventListener("click", (event) => {
                const button = event.target.closest(`[data-show-user-post]`);
                if (button === null) {
                    return;
                }
                button.classList.toggle("active");
                const showUserPost = Number(button.getAttribute("data-show-user-post"));
                callback(showUserPost);
            });
        };
        this.renderPostTable = (posts, id) => {
            const tbody = this.elements.tbody;
            const activePostsRow = tbody.querySelector(`[data-show-user-post=${id}]`).closest("tr");
            const tr = document.createElement("tr");
            tr.innerHTML = `
      <td colspan="100">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Title</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </td>
    `;
            activePostsRow.insertAdjacentElement("afterend", tr);
            const rows = posts.map((post) => {
                const tr = document.createElement("tr");
                tr.innerHTML = `
          <td>${post.title}</td>
          <td>${post.body}</td>
        `;
                return tr;
            });
            tr.querySelector("tbody").append(...rows);
        };
        this.app = appContainer;
        this.model = model;
        this.render();
        this.elements = {
            tableTab: new bootstrap.Tab(document.querySelector("#pills-table-tab")),
            tbody: this.app.querySelector("#pills-table tbody"),
            deleteModal: new bootstrap.Modal(this.app.querySelector("#delete-user-modal")),
            confirmDeleteButton: this.app.querySelector("#confirm-delete"),
            form: this.app.querySelector("#add-user-form"),
            inputName: this.app.querySelector("#input-name"),
            inputUsername: this.app.querySelector("#input-username"),
        };
        this.elements.tableTab.show();
    }
}
exports.UsersView = UsersView;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersViewState = void 0;
class UsersViewState {
    constructor(state) {
        this.users = state.users || [];
        this.removeId = state.removeId || null;
        this.stateChangeCallback = () => { };
    }
    setChangeCallback(callback) {
        this.stateChangeCallback = callback;
    }
    setUsers(users) {
        this.users = users;
        this.stateChangeCallback(this.state);
    }
    setRemoveId(removeId) {
        this.removeId = removeId;
        this.stateChangeCallback(this.state);
    }
    get state() {
        return {
            users: this.users,
            removeId: this.removeId,
        };
    }
}
exports.UsersViewState = UsersViewState;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
class UsersController {
    constructor(model, view) {
        this.handleToggleUser = (id) => {
            const users = this.model.users;
            users.forEach((user) => {
                if (user.id === id) {
                    user.checked = !user.checked;
                }
            });
        };
        this.handleRemoveUser = (id) => {
            this.view.elements.deleteModal.show();
            this.model.setRemoveId(id);
        };
        this.handleConfirmRemove = () => {
            const removeId = this.model.removeId;
            const users = this.model.users;
            if (removeId === null) {
                return;
            }
            const index = users.findIndex((user) => user.id === removeId);
            if (index === -1) {
                return;
            }
            users.splice(index, 1);
            this.model.setUsers(users);
            this.model.setRemoveId(null);
            this.view.elements.deleteModal.hide();
        };
        this.handleAddUser = (newUser) => {
            const users = this.model.users;
            users.push(newUser);
            this.model.setUsers(users);
            this.view.elements.form.reset();
            this.view.elements.tableTab.show();
        };
        this.handlePostUser = (id) => {
            const posts = this.model.posts;
            const userPosts = posts.filter((post) => post.userId === id);
            this.model.setPosts(userPosts);
        };
        this.model = model;
        this.view = view;
        this.model.bindUsersChange(this.view.renderTable);
        this.view.bindToggleUser(this.handleToggleUser);
        this.view.bindRemoveUser(this.handleRemoveUser);
        this.view.bindConfirmRemove(this.handleConfirmRemove);
        this.view.bindAddUser(this.handleAddUser);
        this.view.renderTable(this.model.users);
        this.model.bindUsersPosts(this.view.renderPostTable);
        this.view.bindShowUserPosts(this.handlePostUser);
    }
}
exports.UsersController = UsersController;

import { UsersController } from "./Views/Users/UsersController.js";
import { UsersModel } from "./Views/Users/UsersModel.js";
import { UsersView } from "./Views/Users/UsersView.js";
import { fetchData, fetchPost } from "./Views/Users/api/api.js";

const users: any = await fetchData();
const posts: any = await fetchPost();

const model: UsersModel = new UsersModel({ users, posts });

const app: HTMLElement | null = document.querySelector("#users-view");
const view: UsersView = new UsersView(app, "table-one");

new UsersController(model, view);


export const uuidv4: () => number = (): number => {
  return new Date().getTime();
};
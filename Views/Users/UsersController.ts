export class UsersController {
  private model: any;
  private view: any;

  constructor(model: any, view: any) {
    this.model = model;
    this.view = view;

    this.model.bindUsersChange(this.view.renderTable);
    this.view.bindToggleUser(this.handleToggleUser);
    this.view.bindRemoveUser(this.handleRemoveUser);
    this.view.bindConfirmRemove(this.handleConfirmRemove);
    this.view.bindAddUser(this.handleAddUser);
    this.view.renderTable(this.model.users);

    this.model.bindUsersPosts(this.view.renderPostTable);
    this.view.bindShowUserPosts(this.handlePostUser);
  }

  private handleToggleUser = (id: string | number): void => {
    const users: Array<{ id: string | number; checked?: boolean }> = this.model.users;
    users.forEach((user) => {
      if (user.id === id) {
        user.checked = !user.checked;
      }
    });
  };

  private handleRemoveUser = (id: string | number): void => {
    this.view.elements.deleteModal.show();
    this.model.setRemoveId(id);
  };

  private handleConfirmRemove = (): void => {
    const removeId: string | number | null = this.model.removeId;
    const users: Array<{ id: string | number }> = this.model.users;

    if (removeId === null) {
      return;
    }

    const index: number = users.findIndex((user) => user.id === removeId);

    if (index === -1) {
      return;
    }

    users.splice(index, 1);
    this.model.setUsers(users);

    this.model.setRemoveId(null);
    this.view.elements.deleteModal.hide();
  };

  private handleAddUser = (newUser: any): void => {
    const users: Array<any> = this.model.users;

    users.push(newUser);
    this.model.setUsers(users);

    this.view.elements.form.reset();
    this.view.elements.tableTab.show();
  };
  
  private handlePostUser = (id: string | number): void => {
    const posts: Array<{ userId: string | number }> = this.model.posts;
    const userPosts: Array<any> = posts.filter((post) => post.userId === id);

    this.model.setPosts(userPosts);
  };
}
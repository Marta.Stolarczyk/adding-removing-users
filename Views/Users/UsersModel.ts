export class UsersModel {
  users: Array<any>;
  removeId: number | null;
  posts: Array<any>;
  onUsersChange: ((users: Array<any>) => void) | null;
  onUsersPosts: ((posts: Array<any>) => void) | null;

  constructor(initialState: { users?: Array<any>; removeId?: number | null; posts?: Array<any> }) {
    this.users = initialState.users || [];
    this.removeId = initialState.removeId || null;
    this.posts = initialState.posts || [];
    this.onUsersChange = null;
    this.onUsersPosts = null;
  }

  bindUsersChange(callback: (users: Array<any>) => void): void {
    if (typeof callback === "function") {
      this.onUsersChange = callback;
    }
  }

  bindUsersPosts(callback: (posts: Array<any>) => void): void {
    if (typeof callback === "function") {
      this.onUsersPosts = callback;
    }
  }

  setUsers(users: Array<any>): void {
    this.users = users;
    if (this.onUsersChange) {
      this.onUsersChange(this.users);
    }
  }

  setRemoveId(removeId: number | null): void {
    this.removeId = removeId;
  }

  setPosts(posts: Array<any>): void {
    this.posts = posts;
    if (this.onUsersPosts) {
      this.onUsersPosts(this.posts);
    }
  }

  getPostsByUserId(userId: number): Array<any> {
    return this.posts.filter(post => post.userId === userId);
  }
}
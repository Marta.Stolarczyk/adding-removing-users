export const mapUsers = (users: Array<{ [key: string]: any }>): Array<{ [key: string]: any; checked: boolean }> => {
  const mapedUsers: Array<{ [key: string]: any; checked: boolean }> = users.map((user: { [key: string]: any }) => {
    return {
      ...user,
      checked: false,
    };
  });
  return mapedUsers;
};
import { handleError } from "../../../error/error.js";
import { mapUsers } from "./mapUsers.js";

export const fetchData = async (): Promise<any[]> => {
  try {
    const res: Response = await fetch("https://jsonplaceholder.typicode.com/users");

    if (!res.ok) {
      throw new Error("Something went wrong!");
    }

    const data: unknown = await res.json();
    const users: any[] = mapUsers(data as any);
    return users;
  } catch (error) {
    handleError(error as Error);
  }
};

export const fetchPost = async (): Promise<any[]> => {
  try {
    const response: Response = await fetch(`https://jsonplaceholder.typicode.com/posts`);

    if (!response.ok) {
      throw new Error("Something went wrong!");
    }

    const data: unknown = await response.json();
    return data as any[];
  } catch (error) {
    handleError(error as Error);
  }
};
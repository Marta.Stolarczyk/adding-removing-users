import { uuidv4 } from "../../utils/id-generator.js";
import { actions } from "./actions.js";

interface User {
  id: string;
  name: string;
  username: string;
  checked: boolean;
}

interface UsersViewProps {
  appContainer: HTMLElement;
  model: unknown; // Replace 'unknown' with the actual type of model if available
}

interface Elements {
  tableTab: bootstrap.Tab;
  tbody: HTMLTableSectionElement;
  deleteModal: bootstrap.Modal;
  confirmDeleteButton: HTMLButtonElement;
  form: HTMLFormElement;
  inputName: HTMLInputElement;
  inputUsername: HTMLInputElement;
}

export class UsersView {
  private app: HTMLElement;
  private model: unknown; // Replace 'unknown' with the actual type of model if available
  private elements: Elements;

  constructor({ appContainer, model }: UsersViewProps) {
    this.app = appContainer;
    this.model = model;
    this.render();

    this.elements = {
      tableTab: new bootstrap.Tab(document.querySelector("#pills-table-tab") as HTMLElement),
      tbody: this.app.querySelector("#pills-table tbody") as HTMLTableSectionElement,
      deleteModal: new bootstrap.Modal(this.app.querySelector("#delete-user-modal") as HTMLElement),
      confirmDeleteButton: this.app.querySelector("#confirm-delete") as HTMLButtonElement,
      form: this.app.querySelector("#add-user-form") as HTMLFormElement,
      inputName: this.app.querySelector("#input-name") as HTMLInputElement,
      inputUsername: this.app.querySelector("#input-username") as HTMLInputElement,
    };
    this.elements.tableTab.show();
  }

  render = (): void => {
    const nav: string = `
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item" role="presentation">
        <button
          class="nav-link active"
          id="pills-table-tab"
          data-bs-toggle="pill"
          data-bs-target="#pills-table"
          type="button"
          role="tab"
          aria-controls="pills-table"
          aria-selected="true"
        >
          table
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button
          class="nav-link"
          id="pills-form-tab"
          data-bs-toggle="pill"
          data-bs-target="#pills-form"
          type="button"
          role="tab"
          aria-controls="pills-form"
          aria-selected="false"
        >
          form
        </button>
      </li>
    </ul>
    `;

    const tabContent: string = `
    <div class="tab-content" id="pills-tab-content">
      <div
        class="tab-pane fade show active"
        id="pills-table"
        role="tabpanel"
        aria-labelledby="pills-table-tab"
        tabindex="0"
      >
        <div class="my-3">
          <h3>Table actions</h3>
          <button class="btn btn-primary">Action 1</button>
          <button class="btn btn-primary">Action 2</button>
          <button class="btn btn-primary">Action 3</button>
        </div>

        <table class="table table-bordered">
          <thead>
            <tr>
              <th><input type="checkbox"></th>
              <th>Name</th>
              <th>Username</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>

      <div
        class="tab-pane fade"
        id="pills-form"
        role="tabpanel"
        aria-labelledby="pills-form-tab"
        tabindex="0"
      >
        <h3>Add new user</h3>

        <form id="add-user-form">
          <div class="row">
            <div class="col-3">
              <input type="text" class="form-control" id="input-name" placeholder="Name" required />
            </div>
            <div class="col-3">
              <input
                type="text"
                class="form-control"
                id="input-username"
                placeholder="Username"
                required
              />
            </div>
          </div>

          <div class="row mt-3">
            <div class="col">
              <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>`;

    const deleteModal: string = `
    <div class="modal" tabindex="-1" id="delete-user-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Potwierdzenie usunięcia użytkownika</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div class="modal-body">
            <p>Czy napewno chcesz usunąć tego użytkownika?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Anuluj</button>
            <button type="button" class="btn btn-primary" id="confirm-delete">Potwierdź</button>
          </div>
        </div>
      </div>
    </div>`;

    this.app.innerHTML = nav + tabContent + deleteModal;
  };

  renderTable = (users: User[]): void => {
    const tbody: HTMLTableSectionElement = this.elements.tbody;

    tbody.innerHTML = "";

    const rows: HTMLTableRowElement[] = users.map((user: User) => {
      const tr: HTMLTableRowElement = document.createElement("tr");
      const isChecked: string = user.checked ? "checked" : "";

      tr.innerHTML = `
        <td><input type="checkbox" ${actions.TOGGLE_USER}="${user.id}" ${isChecked} /></td>
        <td>${user.name}</td>
        <td>${user.username}</td>
        <td>
        <button data-show-user-post="${user.id}" class="btn btn-outline-danger"><i class="bi bi-chat-dots-fill"></i></button>
        <button ${actions.REMOVE_USER}="${user.id}" class="btn btn-danger"><i class="bi bi-trash"></i></button>
        </td>
      `;
      return tr;
    });
    tbody.append(...rows);
  };

  bindToggleUser = (callback: (id: number) => void): void => {
    this.elements.tbody.addEventListener("click", (event: MouseEvent) => {
      const button: HTMLElement | null = (event.target as HTMLElement).closest(`[${actions.TOGGLE_USER}]`);

      if (button === null) {
        return;
      }

      const id: number = Number(button.getAttribute(actions.TOGGLE_USER));
      callback(id);
    });
  };

  bindRemoveUser = (callback: (id: number) => void): void => {
    this.elements.tbody.addEventListener("click", (event: MouseEvent) => {
      const button: HTMLElement | null = (event.target as HTMLElement).closest(`[${actions.REMOVE_USER}]`);

      if (button === null) {
        return;
      }

      const id: number = Number(button.getAttribute(actions.REMOVE_USER));
      callback(id);
    });
  };

  bindConfirmRemove = (callback: () => void): void => {
    this.elements.confirmDeleteButton.addEventListener("click", callback);
  };

  bindAddUser = (callback: (newUser: User) => void): void => {
    this.elements.form.addEventListener("submit", (event: Event) => {
      event.preventDefault();

      const name: string = this.elements.inputName.value;
      const username: string = this.elements.inputUsername.value;

      const newUser: User = {
        id: uuidv4(),
        name,
        username,
        checked: false,
      };

      callback(newUser);
    });
  };

  bindShowUserPosts = (callback: (showUserPost: number) => void): void => {
    this.elements.tbody.addEventListener("click", (event: MouseEvent) => {
      const button: HTMLElement | null = (event.target as HTMLElement).closest(`[data-show-user-post]`);

      if (button === null) {
        return;
      }

      button.classList.toggle("active");

      const showUserPost: number = Number(button.getAttribute("data-show-user-post"));
      callback(showUserPost);
    });
  };

  renderPostTable = (posts: unknown[], id: number): void => {
    const tbody: HTMLTableSectionElement = this.elements.tbody;
    const activePostsRow: HTMLTableRowElement = tbody.querySelector(`[data-show-user-post=${id}]`).closest("tr") as HTMLTableRowElement;

    const tr: HTMLTableRowElement = document.createElement("tr");
    tr.innerHTML = `
      <td colspan="100">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Title</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </td>
    `;

    activePostsRow.insertAdjacentElement("afterend", tr);

    const rows: HTMLTableRowElement[] = posts.map((post: unknown) => {
      const tr: HTMLTableRowElement = document.createElement("tr");

      tr.innerHTML = `
          <td>${(post as any).title}</td>
          <td>${(post as any).body}</td>
        `;
      return tr;
    });

    tr.querySelector("tbody").append(...rows);
  };
}
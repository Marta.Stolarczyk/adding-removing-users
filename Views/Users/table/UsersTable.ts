import { actions } from "../actions.js";

interface User {
  id: string;
  name: string;
  username: string;
  checked?: boolean;
}

interface State {
  users: User[];
  setChangeCallback: (callback: () => void) => void;
}

export class UsersTable {
  private container: HTMLElement;
  private state: State;
  private tbody: HTMLTableSectionElement;

  constructor(container: HTMLElement, state: State) {
    this.container = container;
    this.state = state;

    this.render();
    this.tbody = this.container.querySelector("tbody") as HTMLTableSectionElement;

    this.renderTable();
    this.state.setChangeCallback(this.renderTable.bind(this));

    this.addListeners();
    this.renderUserPosts();
  }

  private render(): void {
    this.container.innerHTML = `
    <div class="my-3">
      <h3>Table actions</h3>
      <button class="btn btn-primary">Action 1</button>
      <button class="btn btn-primary">Action 2</button>
      <button class="btn btn-primary">Action 3</button>
    </div>

    <table class="table table-bordered">
      <thead>
        <tr>
          <th><input type="checkbox"></th>
          <th>Name</th>
          <th>Username</th>
          <th>Posts</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>`;
  }

  private renderTable(): void {
    const users: User[] = this.state.users;

    this.tbody.innerHTML = "";

    const rows: HTMLTableRowElement[] = users.map((user: User) => {
      const tr: HTMLTableRowElement = document.createElement("tr");
      const isChecked: string = user.checked ? "checked" : "";

      tr.innerHTML = `
        <td><input type="checkbox" ${actions.TOGGLE_USER}="${user.id}" ${isChecked} /></td>
        <td>${user.name}</td>
        <td>${user.username}</td>
        <td><button data-show-user-post="${user.id}" class="btn btn-outline-danger"><i class="bi bi-chat-dots-fill"></i></button></td>
        <td><button ${actions.REMOVE_USER}="${user.id}" class="btn btn-danger"><i class="bi bi-trash"></i></button></td>
      `;
      return tr;
    });

    this.tbody.append(...rows);
  }

  private addListeners(): void {
    this.tbody.addEventListener("click", async (event: MouseEvent) => {
    });
  }

  private renderUserPosts(): void {
    // Implementation for rendering user posts
  }
}
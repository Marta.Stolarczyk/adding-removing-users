export class UsersViewState {
  users: Array<any>;
  removeId: string | null;
  stateChangeCallback: (state: UsersViewState) => void;

  constructor(state: { users?: Array<any>; removeId?: string | null }) {
    this.users = state.users || [];
    this.removeId = state.removeId || null;
    this.stateChangeCallback = () => {};
  }

  setChangeCallback(callback: (state: UsersViewState) => void): void {
    this.stateChangeCallback = callback;
  }

  setUsers(users: Array<any>): void {
    this.users = users;
    this.stateChangeCallback(this.state);
  }

  setRemoveId(removeId: string | null): void {
    this.removeId = removeId;
    this.stateChangeCallback(this.state);
  }

  get state(): { users: Array<any>; removeId: string | null } {
    return {
      users: this.users,
      removeId: this.removeId,
    };
  }
}
export const actions: { [key: string]: string } = {
  REMOVE_USER: "action-remove-user",
  TOGGLE_USER: "action-toggle-user",
};
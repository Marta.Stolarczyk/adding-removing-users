const template = `
<p></p>
<button>+</button>`;

export const Counter = ({ container }) => {
  container.innerHTML = template;

  let count = 0;

  const p = container.querySelector('p');
  p.innerHTML = count;

  const button = container.querySelector('button');
  button.addEventListener('click', () => {
    count++;
    p.textContent = count;
  });
}